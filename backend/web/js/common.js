$(document).ready(function () {
    $('.modal-button').click(function () {
        $('#zzz_modal').modal('show')
            .find('#modal_content')
            .load($(this).attr('value'));
    });

    //действия на логами ошибок
    $('#error_log_action').change(function () {
        var action = $(this).val();
        if(!action){
            $('#error_log_action_button').attr('disabled', true);
        } else {
            $('#error_log_action_button').attr('disabled', false);
        }

        return false;

    });
    $('#error_log_action_button').click(function () {
        var count_selected_rows = $('#w0').yiiGridView('getSelectedRows').length;
        var action = $('#error_log_action').val();
        if(action == 'del_selected' && count_selected_rows == 0) {
            alert('Записи не выбраны');
            return false;
        } else {
            $('#error_log_form').submit();
            return true;
        }
    });
});
