<?php

namespace backend\controllers;

use yii\db\Connection;

/**
 * ProfileController implements the CRUD actions for User model.
 */
class DbExportController extends SiteController
{
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $db_joomla = new Connection([
            'dsn' => 'mysql:host=localhost;dbname=zzz_vpluse',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ]);
        $db_vpluse = new Connection([
            'dsn' => 'mysql:host=localhost;dbname=final_vpluse',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ]);

        //$this->exportUsers($db_joomla, $db_vpluse);
        //$this->exportSections($db_joomla, $db_vpluse);
        //$this->exportCompanies($db_joomla, $db_vpluse);
        //$this->exportRequests($db_joomla, $db_vpluse);
        //$this->exportOutput($db_joomla, $db_vpluse);
        //$this->exportPay($db_joomla, $db_vpluse);
        //$this->exportHistory($db_joomla, $db_vpluse);
        $this->exportGroupsMap($db_joomla, $db_vpluse);
        //$this->templateVars($db_joomla, $db_vpluse);

    }

    private function templateVars($db_joomla, $db_vpluse)
    {

        $var_temp_6 = serialize([
                '#userName#' => 'Имя пользователя',
                '#balance#' => 'Баланс пользователя',
                '#company#' => 'Компния',
                '#note#' => 'Примечание'
            ]
        );
        $var_temp_1 = serialize([
                '#userName#' => 'Имя пользователя',
                '#id#' => 'Номер заявки',
                '#amount#' => 'Сумма вывода',
            ]
        );

        $var_temp_9 = serialize([
                '#user_email#' => 'email пользователя',
                '#user_name#' => 'Имя отправителя',
                '#message_subject#' => 'Тема сообщения',
                '#message_body#' => 'Текст сообщения',
            ]
        );

        $var_temp_14 = serialize([
                '#userName#' => 'Имя пользователя',
                '#title#' => 'Заголовок новости',
                '#text#' => 'Текст новости',
                '#news_url#' => 'URL новости',
                '#profile_url#' => 'URL профиля пользователя'
            ]
        );
        $var_temp_10= serialize([
                '#userName#' => 'Имя пользователя',
                '#id#' => 'Номер заявки',
                '#amount#' => 'Сумма',
                '#payment_detail#' => 'Реквизиты',
            ]
        );
        $var_temp_8= serialize([
                '#userName#' => 'Имя пользователя',
                '#resetLink#' => 'Ссылка для сброса пароля'
            ]
        );

        $var_temp_3= serialize([
                '#userName#' => 'Имя пользователя',
                '#company#' => 'Компания',
                '#section#' => 'Раздел',
                '#account#' => 'Счет',
                //'#status#' => 'Статус',
            ]
        );
        $var_temp_7= serialize([
                '#userName#' => 'Имя пользователя',
                '#credit#' => 'Сумма',
                '#from_where#' => "Откуда/куда",
            ]
        );


        $db_vpluse->createCommand('
          UPDATE email_templates	Browse
          SET var=\'' . $var_temp_3 .'\' 
          WHERE id=5')
            ->execute();
    }

    private function exportGroupsMap($db_joomla, $db_vpluse)
    {
        $users = $db_joomla->createCommand('
          SELECT user_id
          FROM trader3_user_usergroup_map
          WHERE group_id = 9 AND user_id IN (SELECT id FROM trader3_users);')
            ->queryAll();

        $result = 0;
        foreach ($users as $user) {
            $result += $db_vpluse->createCommand()->insert('user_group', [
                'user_id' => $user['user_id'],
                'group_id' => 1
            ])->execute();
        }

        echo '<pre>'; var_dump($result); echo '</pre>'; exit;

    }

    private function exportHistory($db_joomla, $db_vpluse)
    {


        $users = $db_joomla->createCommand('
          SELECT u.id,  u.userid, u.adminid, u.prim, u.summa, u.datecreate, u.isout, u.ref_pay_log_id
          FROM trader3_sttreferlogs AS u
          WHERE u.userid IN (SELECT id FROM trader3_users) LIMIT 5000 OFFSET 95000;')
            ->queryAll();
        $result = 0;
        foreach ($users as $user) {
            $prim_ar = explode('/', $user['prim']);
            if($user['isout'] == 0) {
                $from_where = array_key_exists(0, $prim_ar) ? trim($prim_ar[0]) : '';
                $note = array_key_exists(1, $prim_ar) ? trim($prim_ar[1]) : '';

            } elseif($user['isout'] == 1) {

                $from_where = '';
                $note = $user['prim'];

            } elseif($user['isout'] == 2) {
                if(count($prim_ar) > 2) {
                    $from_where = array_key_exists(0, $prim_ar) ? trim($prim_ar[0]) : '';
                    $from_where .= array_key_exists(1, $prim_ar) ? '/' . trim($prim_ar[1]) : '';
                    $note = array_key_exists(2, $prim_ar) ? trim($prim_ar[2]) : '';
                } else {
                    $from_where = array_key_exists(0, $prim_ar) ? trim($prim_ar[0]) : '';
                    $note = array_key_exists(1, $prim_ar) ? trim($prim_ar[1]) : '';
                }

            }

            $result += $db_vpluse->createCommand()->insert('history', [
                'id' => $user['id'],
                'id_user' => $user['userid'],
                'id_admin' => $user['adminid'],
                'from_where' => $from_where,
                'note' => $note,
                'credit' => $user['summa'],
                'orientation' => $user['isout'],
                'operation_date' => $user['datecreate']

            ])->execute();
            //if($result > 5000) exit($result);
        }

        echo '<pre>'; var_dump($result); echo '</pre>'; exit;

    }


    private function exportPay($db_joomla, $db_vpluse)
    {
        $users = $db_joomla->createCommand('
            SELECT userid, wmz
            FROM `trader3_sttreferbalans`
            WHERE `wmz` != "" AND userid IN (SELECT id FROM trader3_users);')
            ->queryAll();

        $result = 0;
        foreach ($users as $user) {
            $result += $db_vpluse->createCommand()->insert('user_pay', [
                'user_id' => $user['userid'],
                'pay_id' => 1,
                'value' => $user['wmz'],
            ])->execute();
        }
        echo 'wmz: ' . $result . ';<br>';
        $result = 0;
        $users = $db_joomla->createCommand('
            SELECT userid, okp
            FROM `trader3_sttreferbalans`
            WHERE `okp` != "" AND userid IN (SELECT id FROM trader3_users);')
            ->queryAll();

        foreach ($users as $user) {
            $result += $db_vpluse->createCommand()->insert('user_pay', [
                'user_id' => $user['userid'],
                'pay_id' => 6,
                'value' => $user['okp'],
            ])->execute();
        }
        echo 'okp: ' . $result . ';<br>';
        $result = 0;
        $users = $db_joomla->createCommand('
            SELECT userid, YM
            FROM `trader3_sttreferbalans`
            WHERE `YM` != "" AND userid IN (SELECT id FROM trader3_users);')
            ->queryAll();

        foreach ($users as $user) {
            $result += $db_vpluse->createCommand()->insert('user_pay', [
                'user_id' => $user['userid'],
                'pay_id' => 3,
                'value' => $user['YM'],
            ])->execute();
        }
        echo 'YM: ' . $result . ';<br>';
        $result = 0;
        $users = $db_joomla->createCommand('
            SELECT userid, BP
            FROM `trader3_sttreferbalans`
            WHERE `BP` != "" AND userid IN (SELECT id FROM trader3_users);')
            ->queryAll();

        foreach ($users as $user) {
            $result += $db_vpluse->createCommand()->insert('user_pay', [
                'user_id' => $user['userid'],
                'pay_id' => 7,
                'value' => $user['BP'],
            ])->execute();
        }
        echo 'BP: ' . $result . ';<br>';
        $result = 0;
        $users = $db_joomla->createCommand('
            SELECT userid, EXNESS
            FROM `trader3_sttreferbalans`
            WHERE `EXNESS` != "" AND userid IN (SELECT id FROM trader3_users);')
            ->queryAll();

        foreach ($users as $user) {
            $result += $db_vpluse->createCommand()->insert('user_pay', [
                'user_id' => $user['userid'],
                'pay_id' => 8,
                'value' => $user['EXNESS'],
            ])->execute();
        }
        echo 'EXNESS: ' . $result . ';<br>';
        $result = 0;
        $users = $db_joomla->createCommand('
            SELECT userid, payeer
            FROM `trader3_sttreferbalans`
            WHERE `payeer` != "" AND userid IN (SELECT id FROM trader3_users);')
            ->queryAll();

        foreach ($users as $user) {
            $result += $db_vpluse->createCommand()->insert('user_pay', [
                'user_id' => $user['userid'],
                'pay_id' => 5,
                'value' => $user['payeer'],
            ])->execute();
        }
        echo 'payeer: ' . $result . ';<br>';
        $result = 0;
        $users = $db_joomla->createCommand('
            SELECT userid, pm
            FROM `trader3_sttreferbalans`
            WHERE `pm` != "" AND userid IN (SELECT id FROM trader3_users);')
            ->queryAll();

        foreach ($users as $user) {
            $result += $db_vpluse->createCommand()->insert('user_pay', [
                'user_id' => $user['userid'],
                'pay_id' => 2,
                'value' => $user['pm'],
            ])->execute();
        }
        echo 'pm: ' . $result . ';<br>';
        $result = 0;
        $users = $db_joomla->createCommand('
            SELECT userid, skrill
            FROM `trader3_sttreferbalans`
            WHERE `skrill` != "" AND userid IN (SELECT id FROM trader3_users);')
            ->queryAll();

        foreach ($users as $user) {
            $result += $db_vpluse->createCommand()->insert('user_pay', [
                'user_id' => $user['userid'],
                'pay_id' => 4,
                'value' => $user['skrill'],
            ])->execute();
        }
        echo 'pm: ' . $result . ';<br>';
        exit;

    }


    private function exportOutput($db_joomla, $db_vpluse)
    {
        $users = $db_joomla->createCommand('
          SELECT u.id,  u.userid, u.summa, u.wallet, u.ok, UNIX_TIMESTAMP(u.datecreate) AS created_at,
          b.wmz, b.liqpay, b.okp, b.qiwi, b.YM,	b.BP, b.EXNESS, b.skrill, b.neteller, b.payeer, b.pm, b.om
          FROM trader3_sttreferouts AS u
          LEFT JOIN trader3_sttreferbalans AS b ON u.userid = b.userid
          WHERE u.id > 3 AND u.userid IN (SELECT id FROM trader3_users) LIMIT 2000 OFFSET 10002;')
            ->queryAll();

        $result = 0;
        foreach ($users as $user) {
            $result += $db_vpluse->createCommand()->insert('output', [
                'id' => $user['id'],
                'id_user' => $user['userid'],
                'payment_detail' => $user['wallet'] .'/' . $user[$user['wallet']],
                'payment' => $user['wallet'] .'/' . $user[$user['wallet']],
                'amount' => $user['summa'],
                'status' => $user['ok'],
                'created_at' => $user['created_at'],
            ])->execute();
            if($result > 5000) exit($result);
        }

        echo '<pre>'; var_dump($result); echo '</pre>'; exit;

    }

    private function exportRequests($db_joomla, $db_vpluse)
    {
        $users = $db_joomla->createCommand('
          SELECT u.id,  u.userid, u.compid, u.num, u.actived,
          b.razdelid, UNIX_TIMESTAMP(u.datecreate) AS created_at
          FROM trader3_sttreferorders AS u
          LEFT JOIN trader3_sttrefercomps AS b ON u.compid = b.id
          WHERE u.id > 3 AND u.userid IN (SELECT id FROM trader3_users) AND u.compid IN (SELECT id FROM trader3_sttrefercomps);')
            ->queryAll();

        $result = 0;
        foreach ($users as $user) {
            $result += $db_vpluse->createCommand()->insert('user_request', [
                'id' => $user['id'],
                'id_user' => $user['userid'],
                'id_company' => $user['compid'],
                'id_section' => $user['razdelid'],
                'account' => $user['num'],
                'status' => $user['actived'],
                'created_at' => $user['created_at'],
            ])->execute();
        }

        echo '<pre>'; var_dump($result); echo '</pre>'; exit;

    }

    private function exportUsers($db_joomla, $db_vpluse)
    {
        $users = $db_joomla->createCommand('
          SELECT u.id,  u.username, u.email, u.ref_id, u.ref_code, u.name AS first_name, u.password AS password_hash,
          b.bal AS balance, UNIX_TIMESTAMP(u.registerDate) AS created_at
          FROM trader3_users AS u
          LEFT JOIN trader3_sttreferbalans AS b ON u.id = b.userid;')
            ->queryAll();

        $result = 0;
        foreach ($users as $user) {
            $result += $db_vpluse->createCommand()->insert('user', [
                'id' => $user['id'],
                'username' => $user['username'],
                'email' => $user['email'],
                'ref_id' => $user['ref_id'],
                'ref_code' => $user['ref_code'],
                'first_name' => $user['first_name'],
                'password_hash' => $user['password_hash'],
                'balance' => $user['balance'],
                'created_at' => $user['created_at'],
            ])->execute();
        }

        echo '<pre>'; var_dump($result); echo '</pre>'; exit;

    }

    private function exportCompanies($db_joomla, $db_vpluse)
    {
        $sections = $db_joomla->createCommand('
          SELECT id, name, razdelid
          FROM trader3_sttrefercomps;')
            ->queryAll();

        $result = 0;
        foreach ($sections as $section) {
            $result += $db_vpluse->createCommand()->insert('company', [
                'id' => $section['id'],
                'name' => $section['name'],
                'id_section' => $section['razdelid'],

            ])->execute();
        }

        echo '<pre>'; var_dump($result); echo '</pre>'; exit;

    }

    private function exportSections($db_joomla, $db_vpluse)
    {
        $sections = $db_joomla->createCommand('
          SELECT id, name, doptext
          FROM trader3_sttreferrazdels;')
            ->queryAll();

        $result = 0;
        foreach ($sections as $section) {
            $result += $db_vpluse->createCommand()->insert('section', [
                'id' => $section['id'],
                'attention_block' => $section['doptext'],
                'title' => $section['name'],
                'id_base' => 4,
            ])->execute();
        }

        echo '<pre>'; var_dump($result); echo '</pre>'; exit;

    }

}
