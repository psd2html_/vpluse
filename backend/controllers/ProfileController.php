<?php

namespace backend\controllers;

use Yii;
use common\models\Admin;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use common\models\PasswordChangeForm;

/**
 * ProfileController implements the CRUD actions for User model.
 */
class ProfileController extends SiteController
{
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('view', [
            'model' => $this->findModel(Yii::$app->user->id),
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->user->id);
        $model->scenario = Admin::SCENARIO_PROFILE;
        $avatar = $model->avatar;
        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'avatar');
            $model->avatar = $avatar;
            if (isset($file)) {
                $filename = uniqid() . '.' . $file->extension;
                $path = Yii::getAlias("@webroot") . '/uploads/avatar/' . $filename;
                if ($file->saveAs($path)) {
                    $model->avatar = $filename;
                    $current_avatar = $avatar;
                    if($current_avatar) {
                        $model->deleteAvatar($current_avatar);
                    }
                }
            }

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Профиль успешно обновлён.');

                return $this->redirect(['index', 'id' => $model->id]);
            }
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);

    }

    public function actionPasswordChange()
    {
        $user = $this->findModel(Yii::$app->user->id);
        $model = new PasswordChangeForm($user);

        if ($model->load(Yii::$app->request->post()) && $model->changePassword()) {
            Yii::$app->session->setFlash('success', 'Пароль успешно изменён.');

            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('passwordChange', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Admin::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
