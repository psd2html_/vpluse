<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Мой профиль';
?>

<div class="box box-warning">
    <div class="box-header">
        <h3 class="box-title"><?= $this->title ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::button('Редактировать профиль', ['value' => Url::to('/profile/update'), 'class' => 'btn btn-primary modal-button']) ?>
            <?= Html::button('Изменить пароль', ['value' => Url::to('/profile/password-change'), 'class' => 'btn btn-primary modal-button']) ?>
        </p>
        <br>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute'=>'avatar',
                    'value'=>'/uploads/avatar/' . $model->getAvatar(),
                    'format' => ['image',['width'=>'100']],
                    'label' => 'Аватар',
                ],
                'username',
                'email:email',
                'first_name',
                'last_name',
            ],
        ]) ?>
    </div>
</div>