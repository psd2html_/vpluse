<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\rebate\ImportForm */
/* @var $form ActiveForm */
/* @var $report array */
/* @var $validation_errors array */

$this->title = 'Импорт данных из CSV файла';
?>
<div class="box box-warning">
    <div class="box-header">
        <h3 class="box-title"><?= $this->title ?></h3>
    </div>
    <div class="box-body">

        <?php
        if ($validation_errors) {
            ?>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="width: 100px">Строка</th>
                    <th>Ошибка</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($validation_errors as $key => $item) {
                    ?>
                    <tr>
                        <td><span class="badge bg-light-blue"><?= $key ?></span></td>
                        <td style="color: red"><?= $item ?></td>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <br><br>
            <?php
        } elseif ($report) {
            ksort($report);
            reset($report);
            ?>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th style="width: 100px">Статус</th>
                        <th>Компания</th>
                        <th>Счет</th>
                        <th>Сумма</th>
                        <th>Результат</th>
                    </tr>
                </thead>
                <tbody>
            <?php

            foreach ($report as $key => $item) {
                if ($item['type'] == 'error' ) {
                    $color = '#F00';
                    $btn = 'danger';
                    $status = '!Error';
                } else {
                    $color = '#5cb85c';
                    $btn = 'success';
                    $status = '<i class="fa fa-fw fa-check"></i> Ok';
                }
                ?>
                <tr>
                    <td><?= $key ?></td>
                    <td><span class="label label-<?= $btn ?>"><?= $status ?></span></td>
                    <td><?= $item['company'] ?></td>
                    <td><?= $item['account'] ?></td>
                    <td><?= $item['balance'] ?></td>
                    <td style="color: <?= $color ?>;"><?= $item['message'] ?></td>
                </tr>
                <?php
            }
            ?>
                </tbody>
            </table>
            <br><br><br>
            <?php
        } ?>
        <?php $form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data']
        ]); ?>
            <div class="info-box">
                <span class="info-box-icon bg-yellow">
                    <i class="fa fa-files-o"></i>
                </span>
                <div class="info-box-content">
                    <?= $form->field($model, 'csv')->fileInput([
                        'style' => 'opacity: 0; z-index: -1;',

                    ])->label('Выбрать файл', ['class' => 'btn btn-default']); ?>
                    <div class="form-group">
                        <?= Html::submitButton('<i class="fa fa-download"></i> Загрузить', ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
                <div class="bg-red-active color-palette import_message">
                    <span>Файл должен быть в кодировке UTF-8</span>
                </div>
            </div>

        <?php ActiveForm::end(); ?>

    </div>
</div><!-- rebate-import-index -->
