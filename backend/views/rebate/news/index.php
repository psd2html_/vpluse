<?php

use yii\helpers\Html;
use backend\widgets\CustomGridWidget;
use yii\helpers\Url;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\rebate\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
?>
<div class="box box-warning">
    <div class="box-header">
        <h3 class="box-title"><?= $this->title ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::button('Создать новость', ['value' => Url::to('/rebate/news/create'), 'class' => 'btn btn-success modal-button']) ?>
        </p>
        <?= CustomGridWidget::widget([
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                /*[
                    'attribute' => 'img',
                    'format' => 'html',
                    'value' => function($data)
                    {
                        return Html::img('/uploads/news/' . $data->img, ['width' => '100']);
                    },
                ],*/
                'title',
                [
                    'attribute'=>'created_at',
                    'format' => ['date', 'php: d.m.Y'],
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created_at',
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ])
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{send} {update} {delete}',
                    'buttons' => [
                        'update' => function($url){
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', 'javascript:void(0)', [
                                'value' => $url,
                                'class' => 'modal-button']);
                        },
                        'delete' => function($url){
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => 'delete',
                                'data' => [
                                    'confirm' => 'Вы действительно хотите удалить новость?',
                                    'method' => 'post',
                                ],
                            ]);
                        },
                        'send' => function($url){
                            return Html::a('<span class="glyphicon glyphicon-envelope"></span>', $url, ['title' => 'Отправить новость позьзователям']);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>