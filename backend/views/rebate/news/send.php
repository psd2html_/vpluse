<?php
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\SearchUser */
/* @var $searchModelGroup backend\models\rebate\GroupSearch */
/* @var $dataProviderGroup yii\data\ActiveDataProvider */
/* @var $model common\models\rebate\News */


$this->title = 'Рассылка новости: "' . $model->title . '"';
?>
<div class="box box-warning">
    <div class="box-header">
        <h3 class="box-title"><?= $this->title ?></h3>
    </div>
    <div class="box-body">
        <?php
        echo Tabs::widget([
            'items' => [
                [
                    'label' => 'Пользователи',
                    'content' => '<h2>Пользователи</h2>' . $this->render('_users', [
                            'dataProvider' => $dataProvider,
                            'searchModel' => $searchModel,
                        ]),
                    'active' => true
                ],
                [
                    'label' => 'Группы',
                    'content' => '<h2>Группы пользователей</h2>' . $this->render('_groups', [
                            'dataProvider' => $dataProviderGroup,
                            'searchModel' => $searchModelGroup,
                        ])
                ],
            ]
        ]);
        ?>
    </div>
</div>
