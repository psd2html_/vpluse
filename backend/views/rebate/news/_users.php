<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\SearchUser */
?>
<?= Html::beginForm() ?>
<?php Pjax::begin(); ?>
<?= GridView::widget([
    'pager' => [
        'firstPageLabel' => 'Первая страница',
        'lastPageLabel'  => 'Последняя страница'
    ],
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'summary' => 'Показано {count} из {totalCount}',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'yii\grid\CheckboxColumn',
            'name' => 'users[]',
            'checkboxOptions' => function($data) {
                return ['value' => $data['id'] ];
            },
        ],

        'fullName',
        'username'
    ],
]); ?>
<?php Pjax::end(); ?>
<?= Html::submitButton('Отправить новость', ['class' => 'btn btn-primary']) ?>
<?= Html::endForm() ?>