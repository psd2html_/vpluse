<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\rebate\StaticBlock */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="static-block-form">

    <?php $form = ActiveForm::begin(['id' => 'static-block']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'html')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'standard',
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
