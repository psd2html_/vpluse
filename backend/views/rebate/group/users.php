<?php
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $dataProviderOthers yii\data\ActiveDataProvider */
/* @var $searchModel backend\models\rebate\UserGroupSearch */
/* @var $model backend\models\rebate\UserGroup */


$this->title = 'Группа: "' . $model->name . '"';
?>
<div class="box box-warning">
    <div class="box-header">
        <h3 class="box-title"><?= $this->title ?></h3>
    </div>
    <div class="box-body">
    <?php
    echo Tabs::widget([
        'items' => [
            [
                'label' => 'Пользователи, которые входят в группу',
                'content' => '<h2>Пользователи, которые входят в группу</h2>' . $this->render('_on', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    ]),
                'active' => true
            ],
            [
                'label' => 'Пользователи, которые не входят в группу',
                'content' => '<h2>Пользователи, которые не входят в группу</h2>' . $this->render('_off', [
                        'dataProviderOthers' => $dataProviderOthers,
                        'searchModel' => $searchModel,
                    ])
            ],
        ]
    ]);
    ?>
    </div>
</div>
