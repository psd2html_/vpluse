<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel backend\models\rebate\UserGroupSearch */
?>

<?= Html::beginForm() ?>
<?php Pjax::begin(); ?>
<?= Html::submitButton('Удалить из группы', ['class' => 'btn btn-danger']) ?>
<?= GridView::widget([
    'pager' => [
        'firstPageLabel' => 'Первая страница',
        'lastPageLabel'  => 'Последняя страница'
    ],
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'summary' => 'Показано {count} из {totalCount}',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'yii\grid\CheckboxColumn',
            'name' => 'del_users[]',
            'checkboxOptions' => function($data) {
                return ['value' => $data['id'] ];
            },
        ],
        'fullName',
        'username',
        //'login'
    ],
]); ?>
<?php Pjax::end(); ?>
<?= Html::submitButton('Удалить из группы', ['class' => 'btn btn-danger']) ?>
<?= Html::endForm() ?>

