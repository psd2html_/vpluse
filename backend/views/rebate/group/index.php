<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\rebate\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Группы пользователей';
?>
<div class="box box-warning">
    <div class="box-header">
        <h3 class="box-title"><?= $this->title ?></h3>
    </div>
    <div class="box-body">
        <p>
            <?= Html::button('Создать группу', ['value' => Url::to('/rebate/group/create'), 'class' => 'btn btn-success modal-button']) ?>
        </p>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'name',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{users} {update} {delete}',
                    'buttons' => [
                        'update' => function($url){
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', 'javascript:void(0)', [
                                'value' => $url,
                                'class' => 'modal-button']);
                        },
                        'delete' => function($url){
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => 'delete',
                                'data' => [
                                    'confirm' => 'Вы действительно хотите удалить группу?',
                                    'method' => 'post',
                                ],
                            ]);
                        }, //glyphicon glyphicon-user
                        'users' => function($url, $model){
                            return Html::a('<span class="glyphicon glyphicon-user"></span>', Url::to(['/rebate/group/users', 'group_id' => $model->id]), ['title' => 'users']);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>
