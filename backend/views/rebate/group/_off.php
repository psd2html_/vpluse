<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProviderOthers yii\data\ActiveDataProvider */
/* @var $searchModel backend\models\rebate\UserGroupSearch */

?>

<?= Html::beginForm() ?>
<?= Html::submitButton('Добавить в группу', ['class' => 'btn btn-success']) ?>
<?php Pjax::begin(); ?>
<?= GridView::widget([
    'pager' => [
        'firstPageLabel' => 'Первая страница',
        'lastPageLabel'  => 'Последняя страница'
    ],
    'dataProvider' => $dataProviderOthers,
    'filterModel' => $searchModel,
    'summary' => 'Показано {count} из {totalCount}',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'yii\grid\CheckboxColumn',
            'name' => 'add_users[]',
            'checkboxOptions' => function($data) {
                return ['value' => $data['id'] ];
            },
        ],
        'fullName',
        'username',
    ],
]); ?>
<?php Pjax::end(); ?>
<?= Html::submitButton('Добавить в группу', ['class' => 'btn btn-success']) ?>
<?= Html::endForm() ?>


