<aside class="main-sidebar">
    <section class="sidebar">
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Меню', 'options' => ['class' => 'header']],
                    ['label' => 'Заявки на вывод', 'icon' => 'glyphicon glyphicon-bell', 'url' => ['/rebate/output']],
                    ['label' => 'Заявки партнеров', 'icon' => 'glyphicon glyphicon-bell', 'url' => ['/partner/output']],
                    ['label' => 'Баланс клиентов', 'icon' => 'glyphicon glyphicon-briefcase', 'url' => ['/rebate/balance']],
                    ['label' => 'Партнерский баланс', 'icon' => 'glyphicon glyphicon-briefcase', 'url' => ['/partner/balance']],
                    ['label' => 'Список счетов', 'icon' => 'fa fa-table', 'url' => ['/rebate/user-request']],
                    ['label' => 'Базовые разделы', 'icon' => 'fa fa-list', 'url' => ['/rebate/base-section']],
                    ['label' => 'Справочник разделов', 'icon' => 'fa fa-indent', 'url' => ['/rebate/section']],
                    ['label' => 'Справочник компаний', 'icon' => 'fa fa-industry', 'url' => ['/rebate/company']],
                    ['label' => 'Новости', 'icon' => 'fa fa-rss', 'url' => ['/rebate/news']],
                    ['label' => 'Статические блоки', 'icon' => 'glyphicon glyphicon-th-large', 'url' => ['/rebate/static-block']],
                    ['label' => 'Платежные системы', 'icon' => 'fa  fa-usd', 'url' => ['/rebate/pay']],
                    ['label' => 'Журнал', 'icon' => 'glyphicon glyphicon-book', 'url' => ['/rebate/journal']],
                    ['label' => 'Импорт', 'icon' => 'glyphicon glyphicon-import', 'url' => ['/rebate/import']],
                    ['label' => 'Группы', 'icon' => 'glyphicon glyphicon-tag', 'url' => ['/rebate/group']],
                ],
            ]
        ) ?>
        <?php
        if (Yii::$app->user->identity->id == 1) {
            echo dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        ['label' => 'Супер админ', 'options' => ['class' => 'header']],
                        ['label' => 'Настройки сайта', 'icon' => 'glyphicon glyphicon-briefcase', 'url' => ['/configure']],
                        ['label' => 'Шаблоны писем', 'icon' => 'glyphicon glyphicon-briefcase', 'url' => ['/email-templates']],
                        ['label' => 'Пользователи', 'icon' => 'glyphicon glyphicon-briefcase', 'url' => ['/user']],
                        ['label' => 'Системные ошибки', 'icon' => 'glyphicon glyphicon-briefcase', 'url' => ['/system-log']],

                    ],
                ]
            );

        } ?>
    </section>
</aside>
