<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Вход';
?>

<div class="login-box">
    <div class="login-logo">
        <a href="/">Вплюсе</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <?= Alert::widget() ?>
        <p class="login-box-msg">Пожалуйста, заполните ваш адрес электронной почты. Ссылка для сброса пароля будет выслана на него.</p>

        <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

        <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

        <div class="row">
            <div class="col-xs-4">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
            </div>
            <!-- /.col -->
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
