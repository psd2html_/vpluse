<?php
use backend\widgets\CustomGridWidget;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\\SearchUser */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Управление пользователями';
?>
<div class="box box-warning">
    <div class="box-header">
        <h3 class="box-title"><?= $this->title ?></h3>
    </div>
    <div class="box-body">
        <?= CustomGridWidget::widget([
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'id',
                'fullName',
                'username',
                'email:email',
                [
                    'attribute'=>'created_at',
                    'format' => ['date', 'php: d.m.Y'],
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created_at',
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ])
                ],
                //'auth_key',
                //'password_hash',
                //'password_reset_token',
                // 'balance_partner',
                // 'avatar',
                // 'ref_id',
                // 'ref_code',
                // 'status',
                 //'created_at',
                // 'updated_at',
                // 'subscription',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
