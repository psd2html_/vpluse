<?php
use yii\helpers\Url;

$current_action = Yii::$app->controller->action->id;
$current_controller = Yii::$app->controller->id;
$active_balance = '';
if($current_controller == 'pay' || $current_action == 'history') {
    $active_balance = ' active';
}
?>
<div class="sidebar">
    <ul class="sidebar__nav">
        <li class="sidebar__row">
            <a class="sidebar__item sidebar__item-balance<?php echo $current_action == 'index' ? ' active-empty' : ''?>" href="/partner">
                <span class="sidebar__title">Промо материалы</span>
            </a>
        </li>
        <li class="sidebar__row">
            <a class="sidebar__item sidebar__item-balance<?php echo $current_action == 'customers' ? ' active-empty' : ''?>" href="/partner/customers">
                <span class="sidebar__title">Клиенты</span>
            </a>
        </li>
        <li class="sidebar__row">
            <a class="sidebar__item sidebar__item-balance js-drop<?= $active_balance ?>" href="#?">
                <span class="sidebar__title">Начисления</span>
            </a>
            <ul class="dropdown">
                <li class="dropdown__row">
                    <a class="dropdown__item<?php echo $current_action == 'history' ? ' active' : ''?>" href="<?= Url::to(['/partner/history']) ?>">История начислений</a>
                </li>
                <li class="dropdown__row">
                    <a class="dropdown__item<?php echo $current_action == 'output' ? ' active' : ''?>" href="<?= Url::to(['/pay/output']) ?>">Вывести средства</a>
                </li>
                <li class="dropdown__row">
                    <a class="dropdown__item<?php echo $current_action == 'payment-details' ? ' active' : ''?>" href="<?= Url::to(['/pay/payment-details']) ?>"">Платежные реквизиты</a>
                </li>
            </ul>
        </li>
    </ul>
    <a class="show-menu" href="#?">
        <span class="show-menu__ico"></span>
        <span class="show-menu__title">свернуть</span>
    </a>
</div>