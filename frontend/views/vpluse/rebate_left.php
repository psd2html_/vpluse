<?php
use yii\helpers\Url;
use frontend\widgets\SectionMenuWidget;
$current_action = Yii::$app->controller->action->id;
$current_controller = Yii::$app->controller->id;
$active_balance = '';
if($current_controller == 'balance' || $current_controller == 'pay') {
    $active_balance = ' active';
}
//echo '<pre>'; var_dump($current_action); echo '</pre>'; exit;
?>
<div class="sidebar">
    <ul class="sidebar__nav">
        <?= SectionMenuWidget::widget(); ?>
        <li class="sidebar__row">
            <a class="sidebar__item sidebar__item-support<?php echo $current_action == 'support' ? ' active-empty' : ''?>" href="<?= Url::to(['/rebate/default/support']) ?>">
                <span class="sidebar__title">Техподдержка</span>
            </a>
        </li>
        <li class="sidebar__row">
            <a class="sidebar__item sidebar__item-profile<?php echo $current_controller == 'profile' ? ' active-empty' : ''?>" href="<?= Url::to(['/rebate/profile']) ?>">
                <span class="sidebar__title">Мой профиль</span>
            </a>
        </li>
        <li class="sidebar__row">
            <a class="sidebar__item sidebar__item-balance js-drop<?= $active_balance ?>" href="#?">
                <span class="sidebar__title">Баланс</span>
            </a>
            <ul class="dropdown">
                <li class="dropdown__row">
                    <a class="dropdown__item<?php echo $current_controller == 'balance' ? ' active' : ''?>" href="<?= Url::to(['/rebate/balance']) ?>">История операций</a>
                </li>
                <li class="dropdown__row">
                    <a class="dropdown__item<?php echo $current_action == 'output' ? ' active' : ''?>" href="<?= Url::to(['/pay/output']) ?>">Вывести средства</a>
                </li>
                <li class="dropdown__row">
                    <a class="dropdown__item<?php echo $current_action == 'payment-details' ? ' active' : ''?>" href="<?= Url::to(['/pay/payment-details']) ?>">Платежные реквизиты</a>
                </li>
            </ul>
        </li>
    </ul>
    <a class="show-menu" href="#?">
        <span class="show-menu__ico"></span>
        <span class="show-menu__title">свернуть</span>
    </a>
</div>