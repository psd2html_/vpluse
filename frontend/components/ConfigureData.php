<?php
namespace frontend\components;

use yii\base\Component;
use backend\models\Configure;

class ConfigureData extends Component
{
    public function getData()
    {
        $configure = [];
        $configure_zzz = Configure::find()->all();
        if($configure_zzz) {
            foreach ($configure_zzz as $param) {
                $configure[$param->key] = $param->value;
            }
        }

        return $configure;
    }
}