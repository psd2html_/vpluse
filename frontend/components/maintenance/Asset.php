<?php
/**
 * Maintenance mode component asset bundle.
 * @package brussens\maintenance
 * @version 0.2.1
 * @author BrusSENS (Brusenskiy Dmitry) <brussens@nativeweb.ru>
 * @link https://github.com/brussens/yii2-maintenance-mode
 */
namespace frontend\components\maintenance;
use yii\web\AssetBundle;
class Asset extends AssetBundle
{
    public $sourcePath = '@frontend/components/maintenance/assets';
    /*public $css = [
        YII_ENV_DEV ? 'css/styles.css' : 'css/styles.min.css',
    ];*/
    public $css = ['css/styles.css'];
    public $js = [];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}