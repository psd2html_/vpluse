<?php

namespace app\module\rebate\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\rebate\ContactForm;

/**
 * Default controller for the `rebate` module
 */
class DefaultController extends Controller
{
    public $layout = 'vpluse';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionSupport()
    {

        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Спасибо. Мы ответим вам при возможности.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке сообщения.');
            }

            return $this->refresh();
        } else {
            return $this->render('support', [
                'model' => $model,
            ]);
        }
    }

}
