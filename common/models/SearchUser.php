<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * SearchUser represents the model behind the search form about `common\models\User`.
 */
class SearchUser extends User
{
    public $fullName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'fullName'], 'string'],
            [['created_at'], 'date', 'format' => 'php: d.m.Y'],
            ['email', 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $query = null)
    {
        if(is_null($query)) {
            $query = User::find();
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'fullName' => [
                    'asc' => ['first_name' => SORT_ASC, 'last_name' => SORT_ASC],
                    'desc' => ['first_name' => SORT_DESC, 'last_name' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'username',
                'balance',
                'balance_partner',
                'email',
                'created_at'
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->created_at) {
            $mysql_date_start = strtotime($this->created_at);
            $mysql_date_end = strtotime($this->created_at) + (60*60*24);
            $query->andFilterWhere(['>=', 'created_at', $mysql_date_start]);
            $query->andFilterWhere(['<', 'created_at', $mysql_date_end]);
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'username', $this->username]);

        $query->andWhere('first_name LIKE "%' . $this->fullName . '%" ' .
            'OR last_name LIKE "%' . $this->fullName . '%"'
        );

        $query->andFilterWhere(['like', 'email', $this->email]);


        return $dataProvider;
    }
}
