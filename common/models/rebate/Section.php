<?php

namespace common\models\rebate;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "section".
 *
 * @property integer $id
 * @property integer $id_base
 * @property string $title
 * @property string $attention_block
 * @property string $refback_title
 * @property string $refback_html
 *
 * @property CompanySection[] $companySections
 */
class Section extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'section';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'attention_block', 'id_base'], 'required'],
            [['attention_block', 'refback_html'], 'string'],
            [['title', 'refback_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_base' => 'Базовый радел',
            'baseSectionName' => 'Базоый радел',
            'title' => 'Название раздела',
            'attention_block' => 'Блок "Внимание"',
            'refback_title' => 'Заголовок правого блока',
            'refback_html' => 'Html правого блока',
            'menu_title' => 'Заголовок меню'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBaseSection()
    {
        return $this->hasOne(BaseSection::className(), ['id' => 'id_base']);
    }

    /**
     * @return string
     */
    public function getBaseSectionName()
    {
        return $this->baseSection->name;
    }

    /**
     * @return array
     */
    public static function getBaseSectionsList()
    {
        $sections = BaseSection::find()
            ->select(['id', 'name'])
            ->orderBy('name')
            ->all();

        return ArrayHelper::map($sections, 'id', 'name');
    }
}
