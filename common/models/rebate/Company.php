<?php

namespace common\models\rebate;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "company".
 *
 * @property integer $id
 * @property string $id_section
 * @property string $name
 * @property array $section
 *
 * @property CompanySection[] $companySections
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['id_section'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_section' => 'Разделы',
            'name' => 'Название компании',
            'sectionTitle' => 'Разделы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
   /* public function getSection()
    {
        //return $this->hasMany(CompanySection::className(), ['id_company' => 'id']);
        return $this->hasMany(Section::className(), ['id'=>'id_section'])->viaTable('{{%company_section}}', ['id_company'=>'id']);
    }*/
    public function getSection()
    {
        return $this->hasOne(Section::className(), ['id' => 'id_section']);
    }

    /**
     * @return string
     */
    public function getSectionTitle()
    {
        return $this->section->title;
    }

    /**
     * @return array
     */
    public static function getSectionsList()
    {
        $sections = Section::find()
            ->select(['id', 'title'])
            ->orderBy('title')
            ->all();

        return ArrayHelper::map($sections, 'id', 'title');
    }



}
