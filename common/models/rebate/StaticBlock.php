<?php

namespace common\models\rebate;

use Yii;

/**
 * This is the model class for table "static".
 *
 * @property integer $id
 * @property string $name
 * @property string $html
 */
class StaticBlock extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'static';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'html'], 'required'],
            [['html'], 'string'],
            [['name'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название блока',
            'html' => 'Html статического блока',
        ];
    }
}
